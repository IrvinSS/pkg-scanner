# Scanner

*Scan you INE with a simple photo*

## Installation

[Packagist repo](https://packagist.org/packages/irvincode/scanner)

    composer require irvincode/scanner
    
## Documentation

*Call the class with the image to scan*

    use IrivinCode\Scanner\ScanIne;
    
    [...]
    
    $ine = new ScanINE('image/path');
    
### Functions

#### Note: All data returned as a *string*

    getName();
    
Returns full name

    getBirth();
    
Returns the birth
    
    getAddress();
    
Returns full address
    
    getElectorKey();
    
Returns the elector key
    
    getCurp();
    
Returns the curp
    
    getYearRegister();

Returns the year in which it is registered
    
    getState();

Returns the number of the state
    
    getMunicipality();

Returns the number of the municipality
    
    getSection();
    
Returns the number of the section
    
    getLocation();
    
Returns the number of the locality
    
    getEmission();
    
Return the year of the emission
    
    getValidity();
    
Return the year of validity