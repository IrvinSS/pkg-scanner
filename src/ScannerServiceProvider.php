<?php

namespace IrvinCode\Scanner;

use Illuminate\Support\ServiceProvider;

class ScannerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('IrvinCode\Scanner\ScanINE');
    }
}
