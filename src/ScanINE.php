<?php

namespace IrvinCode\Scanner;

use __;
use Google\Cloud\Vision\VisionClient;
use Intervention\Image\ImageManagerStatic as Image;

putenv('GOOGLE_APPLICATION_CREDENTIALS=/home/vagrant/Code/VisionAPI/GoogleVision/client_credentials.json');

class ScanINE
{
    /**
     * Image
     * @var array|mixed
     */
    protected $image = array();

    /**
     * @var string
     */
    protected $check = '28';

    /**
     * Coords X of INSTITUTO
     * @var string
     */
    protected $coordsX = '73';

    /**
     * Coords Y of INSTITUTO
     * @var string
     */
    protected $coordsY = '11';

    /**
     * Coords X of MEXICO
     * @var string
     */
    protected $secundaryCoordsX = '46';

    /**
     * Coords Y of the MEXICO
     * @var string
     */
    protected $secundaryCoordsY = '19';

    /**
     * Card constructor.
     * @param $image
     */
    public function __construct($image)
    {
        Image::make($image)->resize(400, 250)->save($image);
        $this->image = $this->detect_text('ahorrolibre-fake', $image);
        $this->image = $this->image['textAnnotations'];
    }

    /**
     * @param $projectId
     * @param $fileName
     * @return array|null
     */
    public function detect_text($projectId, $fileName)
    {
        $vision = new VisionClient([
            'projectId' => $projectId,
        ]);
        $image = $vision->image(file_get_contents($fileName), ['TEXT_DETECTION']);
        $result = $vision->annotate($image);
        return($result)->info();
    }

    /**
     * @return array
     */
    public function getCoords(){
        $results = __::get( __::filter($this->image, function($n) {
            if($n['description'] == "INSTITUTO") {
                return $n['description'] == "INSTITUTO";
            }
        })[0], 'boundingPoly.vertices');
        $newX = ( $this->coordsX - ($results[2]['x'] - $results[0]['x']) );
        $newY = ( $this->coordsY - ($results[2]['y'] - $results[0]['y']) );
        if($newY == 0 && $results[2]['y'] != $this->check ) {
            $results = __::get( __::filter($this->image, function($n) {
                if($n['description'] == "MÉXICO") {
                    return $n['description'] == "MÉXICO";
                }
            })[0], 'boundingPoly.vertices');
            $newY = ( $this->secundaryCoordsY - ($results[2]['y'] - $results[0]['y']) );
        }
        if($newX <= 1){
            $newX = $newX * -1;
        }
        if($newY <= 1){
            $newY = $newY * -1;
        }
        return [$newX, $newY];
    }

    /**
     * @param $obj
     * @return null|string
     */
    public function getResult($obj){
        $result = null;
        foreach($obj as $key => $val) {
            $result = $result . " " . $val['description'];
        }
        return $result;
    }

    /**
     * @param array $values
     * @return null|string
     */
    public function values_specific(array $values){
        $value = __::filter($this->image, function($n) use ($values){
            return
                $n['boundingPoly']['vertices'][0]['x'] >= (($values[0]) - $this->getCoords()[0]) &&
                $n['boundingPoly']['vertices'][0]['y'] >= (($values[1]) - $this->getCoords()[1]) &&
                $n['boundingPoly']['vertices'][0]['x'] <= (($values[2]) - $this->getCoords()[0]) &&
                $n['boundingPoly']['vertices'][0]['y'] <= (($values[3]) - $this->getCoords()[1]);
        });
        return $this->getResult($value);
    }

    /**
     * @return null|string
     */
    public function getName(){
        $maxCoords = ['55', '73', '275', '110'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getBirth(){
        $maxCoords = ['155', '73', '345', '85'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getAddress(){
        $maxCoords = ['65', '121', '400', '158'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getElectorKey(){
        $maxCoords = ['175', '155', '245', '175'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getCurp(){
        $maxCoords = ['150', '175', '200', '187'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getYearRegister(){
        $maxCoords = ['330', '170', '385', '190'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getState(){
        $maxCoords = ['155', '185', '170', '205'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getMunicipality(){
        $maxCoords = ['235', '185', '260', '205'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getSection(){
        $maxCoords = ['310', '185', '345', '205'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getLocation(){
        $maxCoords = ['160', '205', '185', '220'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getEmission(){
        $maxCoords = ['235', '205', '260', '220'];
        return $this->values_specific($maxCoords);
    }

    /**
     * @return null|string
     */
    public function getValidity(){
        $maxCoords = ['305', '205', '350', '220'];
        return $this->values_specific($maxCoords);
    }
}